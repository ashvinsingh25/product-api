package com.product.manage.controller;

import com.product.manage.entity.ProductEntity;
import com.product.manage.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductServiceImpl service;

    @RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
    public ResponseEntity<List<ProductEntity>> getProducts() {
        service.listAll();
        return ResponseEntity.ok().body(service.listAll());
    }
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<String> saveProduct(@RequestBody  ProductEntity product) {
        service.save(product);
       return ResponseEntity.ok().body("Success");
    }

    @RequestMapping(value = "/edit" , method = RequestMethod.POST)
    public ResponseEntity<String> editProduct(@RequestBody  ProductEntity product) {
          service.save(product);

        return  ResponseEntity.ok().body("Success");
    }

    @RequestMapping("/delete/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable(name = "id") int id) {
        service.delete(id);
        return  ResponseEntity.ok().body("Success");
    }
}
