package com.product.manage.service;

import com.product.manage.entity.ProductEntity;
import com.product.manage.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional

public class ProductServiceImpl {

    @Autowired
    private ProductRepository repo;

    /**
     * This mehtod used for fetch all Products
     * @return
     */
    public List<ProductEntity> listAll() {
        return repo.findAll();
    }

    /**
     * This method used for persist product
     * @param product
     */
    public void save(ProductEntity product) {
        repo.save(product);
    }

    /**
     * Method To Fetch Product by Id
     * @param id
     * @return
     */
    public ProductEntity get(long id) {
        return repo.findById(id).get();
    }

    /**
     * Method to Delete Product by id
     * @param id
     */
    public void delete(long id) {
        repo.deleteById(id);
    }

}
